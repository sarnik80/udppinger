import socket



port = 12000


server_socket =  socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


server_socket.bind(('', port))


print("server is listening on :{}".format(port))




while True :
    received_data , address = server_socket.recvfrom(1024)

    print("connection from:", address)
    print("received data : ", received_data.decode())



    response = ""
    if received_data.strip() == b"ping":
        response = "pong"
    else:
        response = "Invalid message"



    server_socket.sendto(response.encode(), address)
