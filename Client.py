import socket 
import time



# serverIpAddress and Port Number  and Message
ServerIP = '192.168.1.75'
ServerPort = 12000  #Random Port Number
Message = b'ping'  # This is my message 
TimeOut = 1 # Time out in second 



# Create UDP Connection
clientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


for i in range(10):

    Rtt = 0


    print("Sending {!r}".format(Message))

    # start time 
    start_time = time.time()
    clientSocket.sendto(Message, (ServerIP, ServerPort))
    

    
    try:
        #Receive response
        print('Waiting to receive')
        clientSocket.settimeout(TimeOut)
        data, server = clientSocket.recvfrom(2048)
        end_time = time.time()


        Rtt = end_time - start_time
        print(f"RTT:{Rtt} seconds.")


    except socket.timeout:
        print("Request Timed out!")
    else:
        print('Received {!r}'.format(data))
            
       
    



clientSocket.close()
